from django.views import generic
from status.models import Status
from django.views.decorators.cache import cache_page

class Base(generic.ListView):
    template_name = "index.html"
    queryset = Status.objects.all()
    paginate_by =  36

    def get_context_data(self,**kwargs):
        context = super(Base,self).get_context_data(**kwargs)
        status = Status.objects.order_by("?")[0]
        context.update({"status":status})
        return context

index = Base.as_view()


class Detail(generic.DetailView):
    model = Status
    template_name = "detail.html"

detail = cache_page(60*15)(Detail.as_view())

class Terms(generic.TemplateView):
    template_name="terms.html"

terms = Terms.as_view()

class Tag(Base):
    def get_queryset(self,**kwargs):
        qs = super(Tag,self).get_queryset(**kwargs)
        tag = self.kwargs.pop("tag")
        return qs.filter(tags__name__in=[tag])

    def get_context_data(self,**kwargs):
        return super(Base,self).get_context_data(**kwargs)

tags_view =cache_page(60*15)(Tag.as_view())
