from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
     url(r'^page/(?P<page>[0-9]+)$', 'fartapp.views.index', name='page'),
     url(r'^$', 'fartapp.views.index', name='index'),
     url(r'^current/status/(?P<pk>[0-9]+)/$', 'fartapp.views.detail', name='detail'),
     url(r'^current/status/tag/(?P<tag>[\w-]+)/$', 'fartapp.views.tags_view', name='tag'),
     url(r'^admin/', include(admin.site.urls)),
)


if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += patterns('',
                 url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
                             'document_root': settings.MEDIA_ROOT,
                         }),
            )

