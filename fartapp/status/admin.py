from django.contrib import admin
from .models import Status
import requests

class AdminStatus(admin.ModelAdmin):
    list_display  = ("__unicode__","get_image","disabled")
    list_per_page = 20
    actions = ["delete_status_code_error", "disabled"]

    def disabled(self, request, queryset):
        queryset.update(disabled = True)


    def delete_status_code_error(self, request, queryset):
        for i in queryset:
            r = requests.get(i.image_url, stream = False)
            if not r.status_code == requests.codes.ok:
                i.delete()

                
    def get_image(self, obj):
        return """<img src="%s" style="width:100px;"/> """ % obj.image_url
    get_image.allow_tags = True

admin.site.register(Status, AdminStatus)
