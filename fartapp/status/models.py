from django.db import models
from taggit.managers import TaggableManager

class Deleted(models.Manager):
    def get_query_set(self):
        query_set = super(Deleted,self).get_query_set()
        return query_set.filter(disabled=False)


class Status(models.Model):
    image_url = models.URLField(unique=True)
    disabled = models.BooleanField(default = False)
    tags = TaggableManager()
    
    all_objects = models.Manager()
    objects = Deleted()

    class Meta:
        ordering = ("-id",)
    def __unicode__(self):
        return u"Status %s" % self.id
