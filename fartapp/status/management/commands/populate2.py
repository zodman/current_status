from django.core.management.base import BaseCommand, CommandError
from fartapp.status.models import Status
import requests
import re
import logging

log = logging.getLogger(__name__)

class Command(BaseCommand):

    def _resolve_text(self, text):
        u = re.findall('(?:http://|www.)[^"\' ]+', text)
        print (u, text)
        if u:
            try:
                r = requests.get(u[0],allow_redirects=True, stream=True)
                url, mimetype = r.url, r.headers["content-type"]
                log.info(url)
                if 'image'  in mimetype:
                    status = Status(image_url = url)
                    status.save()
            except:
                pass


    def handle(self, *args, **options):
        count = args[0]
        r = requests.get("https://api.twitter.com/1/statuses/user_timeline.json?include_entities=true&include_rts=true&screen_name=current_status&count=%s" %count)
        print r.text[0]
        results = r.json()
        for twit in results:
            text =twit.get("text")
            self._resolve_text(text)




