from django.core.management.base import BaseCommand, CommandError
from fartapp.status.models import Status
import requests
import re
import logging

log = logging.getLogger(__name__)

class Command(BaseCommand):

    def _resolve_text(self, text):
        u = re.findall('(?:http://|www.)[^"\' ]+', text)
        print (u, text)
        if u:
            try:
                r = requests.get(u[0],allow_redirects=True, stream=True)
                url, mimetype = r.url, r.headers["content-type"]
                log.info(url)
                if 'image'  in mimetype:
                    status = Status(image_url = url)
                    status.save()
            except:
                pass


    def handle(self, *args, **options):
        count = args[0]
        for page in range(0, int(count)):
            p= {'q':'current status','page':page,"result_type":"mixed"}
            search = requests.get("http://search.twitter.com/search.json", params=p)
            d = search.json()
            if u"error" in d:
                continue
            results = d.get("results",[])
            if not results:
                continue
            for res in results:
                text = res.get("text")
                self._resolve_text(text)



