from django.core.management.base import BaseCommand, CommandError
from fartapp.status.models import Status
import requests
import re
import logging

log = logging.getLogger(__name__)

class Command(BaseCommand):

    def handle(self, *args, **options):
        queryset = Status.objects.all()
        for i in queryset:
            r = requests.get(i.image_url, stream = True)
            if not r.status_code == requests.codes.ok:
                print "delete %s" % i.image_url
                i.delete()
           

