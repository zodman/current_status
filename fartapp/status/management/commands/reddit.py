from django.core.management.base import BaseCommand, CommandError
from fartapp.status.models import Status
import re
import logging
from webscraping import download, xpath
log = logging.getLogger(__name__)

class Command(BaseCommand):
    def handle(self, *args, **options):
        d = download.Download()
        html = d.get("http://www.reddit.com/r/reactiongifs")
        urls = xpath.search(html,"//a[@class='title']/@href")
        for url in urls:
            status = Status(image_url = url)
            status.save()

    


