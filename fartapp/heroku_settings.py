from settings import * 

DEBUG = True
TEMPLATE_DEBUG = DEBUG
import dj_database_url
DATABASES = {'default': dj_database_url.config(default='postgres://localhost')}

