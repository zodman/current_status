from django.contrib.sites.models import get_current_site
from django.conf import settings

def default(request):
    return {
        'SITE':get_current_site(request),
        'FACEBOOK_ID':settings.FACEBOOK_ID,
        'FACEBOOK_SECRET_KEY':settings.FACEBOOK_SECRET_KEY,
        'FACEBOOK_NAMESPACE':settings.FACEBOOK_NAMESPACE,
        'url_suffix':'/page',
    }
