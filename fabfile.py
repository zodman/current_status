from fabric.api import local, settings, show,env, get, run, prompt, cd, lcd
env.hosts = ["depamap.com"]

PATH = "/home/zodman/current_status/"

def __remote_virtualenv(command):
    VIRTUAL_ENV = "/home/zodman/.virtualenvs/status/"
    source = '. %s/bin/activate && ' % VIRTUAL_ENV
    run(source + command)

def clear_memcached():
    run("""echo "flush_all" | nc -q 2 localhost 11211 """)

def populate():
    with cd(PATH):
        __remote_virtualenv("python manage.py populate 3")
        __remote_virtualenv("python manage.py populate2 100")
        __remote_virtualenv("python manage.py unpopulate")

def deploy():
    with cd(PATH):
        run("git reset --hard HEAD")
        run("git pull")
        __remote_virtualenv("python manage.py migrate")
        run("touch fartapp/wsgi.py")
        clear_memcached()

